# KingTrade Integration API

Для работы с API Вам нужно перейти в раздел [Загрузки](https://bitbucket.org/kingtradebo/kingtrade-cs-integration/downloads) и выбрать _"Скачать репозиторий"_.

Далее поместите содержимое директории `src` в любое удобнобное для Вас место в проекте, например `lib/KingTrade`.

## Зависимости

Для корректной работы требуется наличие пакета [Newtonsoft.Json](https://www.nuget.org/packages/Newtonsoft.Json).

## Примеры использования

В многих примерах ниже используется ключевое слово `await`, поэтому Ваш метод, содержащий
обращения к такому методу API, должен быть асинхронным (`async`).

Простронства имён:

* `KingTrade` - Содержит класс `KingTradeAPI`.
* `KingTrade.Structures` - содержит структуры, используемые API.
* `KingTrade.WebSocket` - содержит системный класс KingTradeSocket.

В большинстве случаев требуется первые два пространства имён:

```cs
using KingTrade;
using KingTrade.Structures;
```

### KingTradeAPI.Login(string email, string password)

```cs
var response = await KingTradeAPI.Login("username@example.com", "example-password");
switch (response.status) {
    case LoginResponseStatus.Success:
        Console.WriteLine("Вход успешно выполнен.");
        break;
    case LoginResponseStatus.TFA:
        Console.WriteLine("Требуется код из СМС, см. раздел KingTradeAPI.LoginTFA().");
        break;
    case LoginResponseStatus.IncorrectLogin:
        Console.WriteLine("Неверный логин или пароль.");
        break;
    case LoginResponseStatus.MultiaccountBan:
        Console.WriteLine("Ваш профиль связан с мультиаккаунтами, за подробностями вы можете обратиться в тех. поддержку KingTrade.");
        break;
    case LoginResponseStatus.AgreementBan:
        Console.WriteLine("Вы нарушили одно из правил пользовательского соглашения, за подробностями вы можете обратиться в тех. поддержку KingTrade.");
        break;
    case LoginResponseStatus.ManualBan:
        Console.WriteLine($"Ваш аккаунт был заблокирован. Причина: {response.banReason}.");
        break;
}
```

### KingTradeAPI.LoginTFA(string code)

```cs
var responseStatus = await KingTradeAPI.LoginTFA("010101");
switch (responseStatus) {
    case LoginTFAStatus.Success:
        Console.WriteLine("Вход успешно выполнен.");
        break;
    case LoginTFAStatus.IncorrectTFA:
        Console.WriteLine("Введён неверный код двухфакторной авторизации.");
        break;
}
```

### KingTradeAPI.GetAssets(bool onlyActive = false)

```cs
var allAssets = await KingTradeAPI.GetAssets();
var activeAssets = await KingTradeAPI.GetAssets(true);
/*
asset = {
    name      string         Название актива
    profit    int            Доходность актива в %
    active    bool           Является ли актив доступным для торговли
    category  AssetCategory  Категория актива (Currency | Crypto)
}
*/
```

### KingTradeAPI.Connect(), event KingTradeAPI.OnConnected()

Данный метод выполняет подключение к платформе, при успешном подключении вызывает событие `OnConnected`.

```cs
KingTradeAPI.OnConnected += () => {
    Console.WriteLine("Успешно подключено к платформе");
};

KingTradeAPI.Connect();
```

### KingTradeAPI.Close(), event KingTradeAPI.OnClosed()

Данный метод выполняет подключение к платформе, при успешном подключении вызывает событие `OnConnected`.

```cs
KingTradeAPI.OnClosed += () => {
    Console.WriteLine("Клиент был отключён от платформы");
};

KingTradeAPI.Close();
```

### KingTradeAPI.SelectAsset(string asset)

Данный метод устанавливает текущий актив.
Метод должен быть вызван после события `OnConnected`.

```cs
KingTradeAPI.OnConnected += async () => {
    await KingTradeAPI.SelectAsset("Ripple");
};
```

### KingTradeAPI.MakeBet(MakeBetRequest info)

Данный метод отправляет запрос на создание сделки для текущего актива.
Метод должен быть вызван после события `OnConnected` и установки текущего актива.

```cs
KingTradeAPI.OnConnected += async () => {
    await KingTradeAPI.MakeBet(new MakeBetRequest {
        amount = 1,
        direction = BetDirection.Down,
        account = PlatformAccount.Demo,
        time = 60 // 1 мин в секундах
    });
};
```

### Task KingTradeAPI.closeEvent

Объект типа `Task`, который будет завершён после закрытыя соединения с платформой и перед вызовом события `OnClosed`.

Может быть использован для удержания другого потока/задачи от завершения, возвращает `null`.

```cs
class Program {
    static void Main (string[] args) {
        MainAsync().GetAwaiter().GetResult();
    }

    static async Task MainAsync () {
        await KingTradeAPI.Login("username@example.com", "example-password");

        KingTradeAPI.OnConnected += async () => {
            // Здесь может быть какой-либо асинхронный код
            KingTradeAPI.Close();
        };

        await KingTradeAPI.Connect();
        await KingTradeAPI.closeEvent;
    }
}
```

### event KingTradeAPI.OnBetAdded(BetAddMessage)

Данное событие вызывается после успешного открытия сделки, _не обязательно сделанной через данный клиент_.

```cs
KingTradeAPI.OnBetAdded += deal => {
    /*
    deal = {
        id              string           Идентификатор сделки в БД торгового терминала
        account         PlatformAccount  Аккаунт, на который была поставлена сделка (Real | Demo)
        amount          float            Сумма сделки
        direction       BetDirection     Направление сделки (Up | Down)
        startTimestamp  long             Unix Timestamp времени начала сделки
        startValue      float            Значение актива во время открытия сделки
        endTimestamp    long             Unix Timestamp времени окончания сделки
    }
    */
};
```

### event KingTradeAPI.OnBetClosed(BetCloseMessage)

Данное событие вызывается после успешного открытия сделки, _не обязательно сделанной через данный клиент_.

```cs
KingTradeAPI.OnBetClosed += deal => {
    /*
    deal = {
        id        string           Идентификатор сделки в БД торгового терминала
        account   PlatformAccount  Аккаунт, на который была поставлена сделка (Real | Demo)
        balance   float            Баланс аккаунта пользователя после закрытия сделки
        result    float            Результат сделки
        endValue  float            Значение актива во время закрытия сделки
    }
    */
};
```

### event KingTradeAPI.OnTick(AssetTick)

Данное событие вызывается при получении котировок актива (приблизительно раз в секунду).

```cs
KingTradeAPI.OnTick += tick => {
    /*
    tick = {
        timestamp  long    Unix Timestamp текущего тика
        asset      string  Текущий актив
        value      float   Значение актива в момент `timestamp`
        buy        float   Цена покупки актива в момент `timestamp`
        sell       float   Цена продажи актива в момент `timestamp`
    }
    */
};
```

### event KingTradeAPI.OnError(JToken)

Данное событие вызывается при получении ошибки от торгового терминала.

Это событие может быть полезным для отладки или составлении отчёта об ошибке для разрабочиков торгового терминала.

```cs
KingTradeAPI.OnTick += err => {
    Console.WriteLine("Произошла непредведенная ошибка: " + err);
    // err - чаше всего JSON представление объекта ошибки, либо строка с текстом ошибки
};
```
