using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using KingTrade.Structures;
using KingTrade.WebSocket;

namespace KingTrade {
	class KingTradeAPI {
		public const string BASE_URL = "https://api.kingtrade.pro";

		public static async Task<KTResponse> SendRequest (string path, JObject data = null) {
			if (path[0] == '/') path = path.Substring(1);
			var req = WebRequest.Create(BASE_URL + "/" + path.Replace('.', '/'));
			if (session != null) req.Headers.Add("Session", session);

			if (data == null) {
				req.Method = "GET";
			} else {
				var dataBytes = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(data));
				req.Method = "POST";
				req.ContentLength = dataBytes.Length;
				req.ContentType = "application/json";

				var reqStream = await req.GetRequestStreamAsync();
				await reqStream.WriteAsync(dataBytes, 0, dataBytes.Length);
			}

			HttpWebResponse res;
			try {
				res = (HttpWebResponse) await req.GetResponseAsync();
			} catch (WebException ex) {
				if (ex.Status == WebExceptionStatus.ProtocolError) {
					res = (HttpWebResponse) ex.Response;
				} else {
					throw ex;
				}
			}

			var resStream = res.GetResponseStream();
			var resBytes = new byte[res.ContentLength];
			var readed = 0;
			do {
				readed += await resStream.ReadAsync(resBytes, readed, (int) res.ContentLength - readed);
			}
			while (readed < res.ContentLength);

			var result = new KTResponse {
				code = (int) res.StatusCode,
				success = res.StatusCode == HttpStatusCode.OK,
				session = res.Headers.Get("Session")
			};

			switch (res.ContentType) {
				case "application/json":
					result.msg = JToken.Parse(Encoding.UTF8.GetString(resBytes));
					// result.msg = JsonConvert.DeserializeObject(Encoding.UTF8.GetString(resBytes));
					break;
				default:
					result.msg = Encoding.UTF8.GetString(resBytes);
					break;
			}

			return result;
		}

		internal static string session;
		public static async Task<LoginResponse> Login (string email, string password) {
			var loginData = new JObject();
			loginData.Add("email", email);
			loginData.Add("pass", password);

			var res = await SendRequest("Auth.login", loginData);
			var result = new LoginResponse();

			if (res.success) {
				if ((string) res.msg ==  "TFA") {
					result.status = LoginResponseStatus.TFA;
				} else {
					result.status = LoginResponseStatus.Success;
				}

				session = res.session;
			} else {
				if (typeof(string).IsInstanceOfType(res.msg)) {
					switch ((string) res.msg) {
						case "IncorrectLogin":
							result.status = LoginResponseStatus.IncorrectLogin;
							break;
					}
				} else {
					var resArray = (JArray) res.msg;
					switch ((string) resArray[0]) {
						case "AccountBlocked":
							switch ((string) resArray[1]) {
								case "lng.MultiaccountBan":
									result.status = LoginResponseStatus.MultiaccountBan;
									break;
								case "lng.AgreementBan":
									result.status = LoginResponseStatus.AgreementBan;
									break;
								default:
									result.status = LoginResponseStatus.ManualBan;
									result.banReason = (string) resArray[1];
									break;
							}
							break;
					}
				}
			}

			return result;
		}

		public static async Task<LoginTFAStatus> LoginTFA (string code) {
			var res = await SendRequest("Auth.tfa", new JObject());

			if (res.success) return LoginTFAStatus.Success;
			else return LoginTFAStatus.IncorrectTFA;
		}

		public static async Task<List<AssetInfo>> GetAssets (bool onlyActive = false) {
			var res = await SendRequest("Info.assets");
			if (!res.success) throw new Exception((string) res.msg);

			var result = new List<AssetInfo>();
			foreach (var pair in (JObject) res.msg) {
				var active = (bool) pair.Value["active"];
				if (onlyActive && !active) continue;

				var asset = new AssetInfo {
					name = pair.Key,
					profit = (int) pair.Value["profit"],
					active = active
				};

				switch ((string) pair.Value["category"]) {
					case "AssetCurrency":
						asset.category = AssetCategory.Currency;
						break;
					case "AssetCrypto":
						asset.category = AssetCategory.Crypto;
						break;
				}

				result.Add(asset);
			}

			return result;
		}

		// Socket base section
		public static event Action OnConnected = () => {};
		private static TaskCompletionSource<object> closeEventSource;
		public static Task closeEvent;
		public static async Task Connect () {
			await KingTradeSocket.Connect();
			OnConnected();

			closeEventSource = new TaskCompletionSource<object>();
			closeEvent = closeEventSource.Task;
		}

		public static event Action OnClosed = () => {};
		public static async void Close () {
			await KingTradeSocket.Close();
			closeEventSource.SetResult(null);
			OnClosed();
		}

		public static event Action<AssetTick> OnTick = tick => {};
		public static event Action<JToken> OnError = err => {};
		internal static void DispatchEvent (byte[] buffer) {
			var message = JsonConvert.DeserializeObject<JArray>(Encoding.UTF8.GetString(buffer));
			switch ((string) message[0]) {
				case "session":
					session = (string) message[1];
					break;
				case "error":
					OnError(message[1]);
					break;
				case "tick":
					OnTick(message[1].ToObject<AssetTick>());
					break;
				case "bet":
					switch ((string) message[1]["type"]) {
						case "add":
							ParseBetAdd(message[1]["deal"]);
							break;
						case "close":
							ParseBetClose(message[1]["deal"], (float) message[1]["balance"]);
							break;
					}
					break;
			}
		}

		public static async Task SelectAsset (string asset) {
			await KingTradeSocket.Send("subscribe", (JToken) asset);
		}

		public static async Task MakeBet (MakeBetRequest info) {
			var betObj = new JObject();
			betObj.Add("amount", info.amount);
			betObj.Add("time", info.time);

			switch (info.direction) {
				case BetDirection.Up:
					betObj.Add("direction", "up");
					break;
				case BetDirection.Down:
					betObj.Add("direction", "down");
					break;
			}

			switch (info.account) {
				case PlatformAccount.Real:
					betObj.Add("account", "real");
					break;
				case PlatformAccount.Demo:
					betObj.Add("account", "demo");
					break;
			}

			await KingTradeSocket.Send("bet", betObj);
		}

		// Deals section
		public static event Action<BetAddMessage> OnBetAdded = bet => {};
		internal static void ParseBetAdd (JToken deal) {
			var message = new BetAddMessage {
				id = (string) deal["id"],
				startValue = (float) deal["start"]["value"],
				startTimestamp = (long) deal["start"]["timestamp"],
				endTimestamp = (long) deal["end"]["timestamp"],
				amount = (float) deal["amount"]
			};

			switch ((string) deal["account"]) {
				case "real":
					message.account = PlatformAccount.Real;
					break;
				case "demo":
					message.account = PlatformAccount.Demo;
					break;
			}

			switch ((string) deal["direction"]) {
				case "up":
					message.direction = BetDirection.Up;
					break;
				case "down":
					message.direction = BetDirection.Down;
					break;
			}

			OnBetAdded(message);
		}
		public static event Action<BetCloseMessage> OnBetClosed = bet => {};
		internal static void ParseBetClose (JToken deal, float balance) {
			var message = new BetCloseMessage {
				id = (string) deal["id"],
				endValue = (float) deal["endValue"],
				balance = balance,
				result = (float) deal["result"]
			};

			switch ((string) deal["account"]) {
				case "real":
					message.account = PlatformAccount.Real;
					break;
				case "demo":
					message.account = PlatformAccount.Demo;
					break;
			}

			OnBetClosed(message);
		}
	}
}
