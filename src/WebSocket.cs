using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using System.Text;
using System.Collections.Generic;

namespace KingTrade.WebSocket {
	class KingTradeSocket {
		private static ClientWebSocket ws;
		private static Thread eventsThread;
		private static CancellationToken? receiveToken;
		public static async Task Connect () {
			ws = new ClientWebSocket();
			await ws.ConnectAsync(new Uri("wss://api.kingtrade.pro/socket"), new CancellationToken());
			await ws.SendAsync(Encoding.UTF8.GetBytes("session:" + KingTradeAPI.session), WebSocketMessageType.Text, true, new CancellationToken());

			eventsThread = new Thread(async () => {
				while (ws.State == WebSocketState.Open) {
                    receiveToken = new CancellationToken();
					var message = new List<byte>();

					WebSocketReceiveResult receiveStatus;
					do {
                    	var buffer = new ArraySegment<byte>(new byte[128]);
                    	receiveStatus = await ws.ReceiveAsync(buffer, (CancellationToken) receiveToken);
						message.AddRange(buffer);
					}
					while (!receiveStatus.EndOfMessage);

					if (receiveStatus.CloseStatus != null) {
						KingTradeAPI.Close();
						return;
					}

                    receiveToken = null;
					KingTradeAPI.DispatchEvent(message.ToArray());
                }
			});
			eventsThread.Start();
		}

		public static async Task Send (string eventName, JToken data) {
			JArray dataArray;
			if (data.Type == JTokenType.Array) {
				dataArray = (JArray) data;
				dataArray.AddFirst(eventName);
			} else {
				dataArray = new JArray();
				dataArray.Add(eventName);
				dataArray.Add(data);
			}

			var dataBytes = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(dataArray));
			await ws.SendAsync(dataBytes, WebSocketMessageType.Text, true, new CancellationToken());
		}

		public static async Task Close () {
			if (ws.State != WebSocketState.Closed) {
				await ws.CloseAsync(WebSocketCloseStatus.NormalClosure, "", new CancellationToken());
			}
		}
	}
}