namespace KingTrade.Structures {
	public struct KTResponse {
		public int code;
		public bool success;
		public object msg;
		public string session;
	}

	enum LoginResponseStatus {
		Success, IncorrectLogin, TFA,
		MultiaccountBan, AgreementBan, ManualBan
	}
	struct LoginResponse {
		public LoginResponseStatus status;
		public string banReason;
	}

	enum LoginTFAStatus {
		Success, IncorrectTFA
	}

	enum AssetCategory {
		Currency, Crypto
	}
	struct AssetInfo {
		public string name;
		public int profit;
		public bool active;
		public AssetCategory category;
	}

	struct AssetTick {
		public long timestamp;
		public string asset;
		public float value;
		public float buy;
		public float sell;
	}

	enum BetDirection {
		Up, Down
	}
	enum PlatformAccount {
		Real, Demo
	}
	struct MakeBetRequest {
		public float amount;
		public BetDirection direction;
		public PlatformAccount account;
		public int time;
	}
	struct BetAddMessage {
		public string id;
		public float amount;
		public BetDirection direction;
		public PlatformAccount account;
		public long startTimestamp;
		public float startValue;
		public long endTimestamp;
	}
	struct BetCloseMessage {
		public string id;
		public float balance;
		public PlatformAccount account;
		public float result;
		public float endValue;
	}
}